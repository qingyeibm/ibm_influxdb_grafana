# Grafana / influxdb

This work comprises an automation of the following blog entry
https://db-blog.web.cern.ch/blog/luca-canali/2019-02-performance-dashboard-apache-spark

# Terraform

Terraform scripts are provided to create the infrastructure.

# Ansible

Ansible scripts are provided to install and configure influxdb+grahpite and grafana on a VSI.
