# Metrics server for IAE

This module creates the following infrastructre

    1 x subnet
    1 x RHEL7
    1 x VPC public  LoadBalancer on subnet - for IAE/Spark to send metrics to Influxdb/Graphite (port 2003)
    1 x VPC private LoadBalancer on subnet - for user  to access Grafana UI (port 3000)
    1 x security group

The load balancer is configured to forward traffic to port 80 on the two servers on the cd-ssp-subnet and these servers are also configured with a security group suitable for serving an HTTP application.

### Table of Contents

1. [Server Images](##server-images)
2. [Security Group](##security-group)
2. [Virtual Servers](##virtual-servers)
2. [Load Balancer](##load-balancer)
3. [Module Variables](##module-variables)
---

## Server Images

This module uses Ubuntu RHEL 7. Use the `ibmcloud is images` command to check to see what images are available in your region. The image used is set by the `image` variable in the hostsfile.

---

## Security Group

A security group is not created because the customer does not use security groups. The following rules are however required

- for access to the Grafana UI: inbound to the TCP port specified in var.grafana_ui_port (default: 3000)
- for access to the InfluxDB sink: inbound to the TCP port specified in var.influxdb_sink_port (default: 2003)
(- for SSH access : inbound to port TCP/22)

---

## Load Balancer

This creates two load balancers: one private, one public. Each load balancer is composed of four parts:

- Load Balancer
- Load Balancer Back End Pool
- Load Balancer Pool Members
- Load Balancer Listener

### Load Balancer

This creates a load balancer resource.

### Back End Pool

This creates a pool that will route traffic to the virtual server instances. It will also monitor the health of those instances

### Pool Members

This adds the `primary_ipv4_address` of each virtual server to the pool, and will tell the pool to listen on a specific port. In the example we presume an HTTP server listening on port `80`.

### Listener

This creates a listener for the load balancer that will send traffic to the pool. All pool members must be added before the listener will be created

---

## Module Variables

Variable | Type | Description | Default
---------|------|-------------|--------
`ibmcloud_api_key` | string | The IBM Cloud platform API key needed to deploy IAM enabled resources |
`ibm_region` | string | IBM Cloud region where all resources will be deployed | `eu-de`
`zone` | string | IBM Cloud zone where all resources will be deployed | `eu-de-3`
`generation` | String | Generation of VPC | `2`
`resource_group` | string | Name of resource group to create VPC | `truata`
`vpc_name` | string | Name of VPC |
`unique_id` | string | The IBM Cloud platform API key needed to deploy IAM enabled resources |
`ssh_public_key` | string | ssh public key to use for vsi |
`type` | string | Load Balancer type, can be public or private | `public`
`listener_port` | String | Listener port | `80`
`listener_protocol` | string | The listener protocol. Supported values are http, tcp, and https | `http`
`certificate_instance` | string | Optional, the CRN of a certificate instance to use with the load balancer. | ``
`connection_limit` | String | Optional, connection limit for the listener. Valid range 1 to 15000. | `0`
`algorithm` | string | The load balancing algorithm. Supported values are round_robin, or least_connections. This module can be modified to use weighted_round_robin by adding `weight` to the load balancer pool members. | `round_robin`
`protocol` | string | The pool protocol. Supported values are http, and tcp. | `http`
`health_delay` | String | The health check interval in seconds. Interval must be greater than timeout value. | `11`
`health_retries` | String | The health check max retries. | `10`
`health_timeout` | String | The health check timeout in seconds. | `10`
`health_type` | string | The pool protocol. Supported values are http, and tcp. | `http`
`pool_member_port` | String | The port number of the application running in the server member. | `80`
