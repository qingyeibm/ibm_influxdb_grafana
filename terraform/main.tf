##############################################################################
# Provider
##############################################################################
provider ibm {
  ibmcloud_api_key = var.ibmcloud_api_key
  region           = var.ibm_region
  generation       = var.generation
  ibmcloud_timeout = 60
}

##############################################################################
# Data
##############################################################################
data ibm_resource_group group {
  name = var.resource_group
}

data ibm_is_vpc vpc {
  name = var.vpc_name
}

data ibm_is_ssh_key ssh_key {
    name = var.ssh_key_name
}

data ibm_is_image rhel7_image {
    name = var.rhel7_image_name
}

##############################################################################
# Subnets
##############################################################################
resource ibm_is_subnet subnet {
  name            = var.subnet_name
  vpc             = data.ibm_is_vpc.vpc.id
  zone            = var.zone
  total_ipv4_address_count = 64
#  public_gateway = ibm_is_public_gateway.the_gateway.id
}

##############################################################################
# VSIs
##############################################################################
resource ibm_is_instance host {
  name    = var.vsi_name
  image   = data.ibm_is_image.rhel7_image.id 
  profile = var.generation == 1 ? "bc1-4x16" : "bx2-4x16"

  primary_network_interface {
    subnet = ibm_is_subnet.subnet.id
  }

  vpc  = data.ibm_is_vpc.vpc.id
  zone = var.zone
  keys = [data.ibm_is_ssh_key.ssh_key.id]

  //User can configure timeouts
  timeouts {
    create = "90m"
    delete = "30m"
  }
}

##############################################################################
# Floating IP
##############################################################################
resource ibm_is_floating_ip host_floating_ip {
  name   = var.floating_ip_name
  target = ibm_is_instance.host.primary_network_interface.0.id
}

##############################################################################
# Load balancer
##############################################################################
module lb {
  source               = "./lb_module"
  unique_id            = "trua-metrics-pub"
  subnet_list          = [ibm_is_subnet.subnet.id]
  vsi_list             = [for vsi in [ibm_is_instance.host] : { ipv4_address = vsi.primary_network_interface[0].primary_ipv4_address }]
  resource_group_id    = data.ibm_resource_group.group.id
  type                 = "public"
  listener_port        = var.influxdb_sink_port
  listener_protocol    = "tcp"
  certificate_instance = var.certificate_instance
  connection_limit     = var.connection_limit
  algorithm            = var.algorithm
  protocol             = "tcp"
  health_delay         = var.health_delay
  health_retries       = var.health_retries
  health_timeout       = var.health_timeout
  health_type          = var.health_type
  pool_member_port     = var.influxdb_sink_port
}

module lb_priv {
  source               = "./lb_module"
  unique_id            = "trua-metrics-priv"
  subnet_list          = [ibm_is_subnet.subnet.id]
  vsi_list             = [for vsi in [ibm_is_instance.host] : { ipv4_address = vsi.primary_network_interface[0].primary_ipv4_address }]
  resource_group_id    = data.ibm_resource_group.group.id
  type                 = "private"
  listener_port        = var.grafana_ui_port
  listener_protocol    = "http"
  certificate_instance = var.certificate_instance
  connection_limit     = var.connection_limit
  algorithm            = var.algorithm
  protocol             = "http"
  health_delay         = var.health_delay
  health_retries       = var.health_retries
  health_timeout       = var.health_timeout
  health_type          = var.health_type
  pool_member_port     = var.grafana_ui_port
}

