##############################################################################
#  Account variables
##############################################################################
variable ibmcloud_api_key {
   description = "The IBM Cloud platform API key needed to deploy IAM enabled resources"
   type        = string
   default     = ""
}

variable generation {
   description = "Generation of VPC - 1 or 2"
   type        = number
   default     = 1
}

variable ibm_region {
   description = "IBM Cloud region where all resources will be deployed"
   type        = string
   default     = "eu-de"
}

variable resource_group {
   description = "Name for IBM Cloud Resource Group where resources will be deployed"
   type        = string
   default     = "truata"
}

##############################################################################
#  Metrics server variables
##############################################################################
variable grafana_ui_port {
    description = "Grafana web UI port"
    type       = number
    default     = 3000
}

variable influxdb_sink_port {
    description = "Influxdb sink listener port - where monitored service sends its metrics"
    type       = number
    default     = 2003
}

##############################################################################
#  VPC/VSI Variables, etc.
##############################################################################
variable vpc_name {
    description = "Name of VPC where cluster is to be created"
    type        = string
}

variable ssh_key_name {
    description = "Name of SSH key for VSI logon"
    type        = string
}

variable rhel7_image_name {
    description = "Name of VSI image"
    type        = string
    default     = "ibm-redhat-7-6-minimal-amd64-1" // cf. CLI command "ibmcloud is images"
}

variable zone {
    description = "Name of zone"
    type        = string
    default     = "eu-de-3"
}

variable vsi_name {
    description = "VSI name"
    type        = string
    default     = "trua-graf-vsi"
}

variable floating_ip_name {
    description = "Floating IP name"
    type        = string
    default     = "trua-graf-floating-ip"
}

variable subnet_name {
    description = "Subnet name"
    type        = string
    default     = "trua-graf-sn"
}

##############################################################################
# Listener Variables
##############################################################################
variable certificate_instance {
    description = "Optional, the CRN of a certificate instance to use with the load balancer."
    type        = string
    default     = ""
}

variable connection_limit {
    description = "Optional, connection limit for the listener. Valid range 1 to 15000."
    # type        = number
    default     = 0
}

##############################################################################
# Pool Variables
##############################################################################
variable algorithm {
    description = "The load balancing algorithm. Supported values are round_robin, or least_connections. This module can be modified to use weighted_round_robin by adding `weight` to the load balancer pool members."
    type        = string
    default     = "round_robin"
}

variable health_delay {
    description = "The health check interval in seconds. Interval must be greater than timeout value."
    # type        = number
    default     = 11
}

variable health_retries {
    description = "The health check max retries."
    # type       = number
    default     = 10
}

variable health_timeout {
    description = "The health check timeout in seconds."
    # type       = number
    default     = 10    
}

variable health_type {
    description = "The pool protocol. Supported values are http, and tcp."
    type        = string
    default     = "http"
}

